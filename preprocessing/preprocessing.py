
import pandas

from images import getPlayersImagesSrcs, getTeamsImagesSrcs


def getTeams():
    teams = pandas.read_csv("baseballdatabank-2019.2/core/Teams.csv")
    teams = teams[["teamID", "teamIDBR", "name", "lgID", "yearID", "park"]]
    teams = teams[teams["lgID"].isin(["NL", "AL"])]

    parks = pandas.read_csv("baseballdatabank-2019.2/core/Parks.csv")
    parks = parks[["park.name", "state"]]

    # fix different parks with different names
    parks.loc[(parks["park.name"] == "Wrigley Field") & (parks["state"] == "CA"), "park.name"] = "Wrigley Field (LA)"

    # add missing yankee stadium version
    parks = parks.append({
        "park.name": "Yankee Stadium III",
        "state": "NY",
    }, ignore_index=True)

    teams = teams.merge(parks, left_on="park", right_on="park.name")

    teams = teams[["teamID", "teamIDBR", "name", "lgID", "yearID", "state"]]

    teamsImgSrcs = getTeamsImagesSrcs(teams["teamIDBR"])  # WARNING this takes a few minutes
    teams["teamLogoSrc"] = [teamsImgSrcs[team["teamIDBR"]] for _, team in teams.iterrows()]

    teams.columns = ["teamID", "teamIDBR", "teamName", "lgID", "yearID", "state", "teamLogoSrc"]

    return teams


def playerSearchAndInfo(players, teams):
    appearances = pandas.read_csv("baseballdatabank-2019.2/core/Appearances.csv")
    appearances = appearances[appearances["playerID"].isin(players["playerID"])]
    lastTeamPerPlayer = appearances[
        ["playerID",
         "teamID",
         "yearID",
         "lgID"]].groupby("playerID").max(level="yearID").reset_index()
    appearances = None

    lastTeamPerPlayer = lastTeamPerPlayer.merge(teams, on=["teamID", "yearID"])

    playerData = lastTeamPerPlayer.merge(players, on="playerID")
    lastTeamPerPlayer = None

    playerSearchData = playerData[[
        "playerID",
        "nameFirst",
        "nameLast",
        "playerFotoSrc",
        "position",
        "teamName",
        "teamLogoSrc",
        "lgID",
        "state"
    ]]

    # translate league id to human readable
    for lg in ["NL", "AL"]:
        tranl = "National League" if lg == "NL" else "American League"
        playerSearchData.loc[playerSearchData["lgID"] == lg, "lgID"] = tranl

    playerInfo = playerData[[
        "playerID",
        "nameFirst",
        "nameLast",
        "playerFotoSrc",
        "teamName",
        "teamLogoSrc",
        "birthDay",
        "birthMonth",
        "birthYear",
        "position",
        "bats",
        "throws"
    ]]

    return playerSearchData.drop_duplicates(), playerInfo.drop_duplicates()


def gamesPerTeam(playerIDs, teams):
    appearances = pandas.read_csv("baseballdatabank-2019.2/core/Appearances.csv")
    appearances = appearances[appearances["playerID"].isin(playerIDs)]
    totalGamesPerPlayerPerTeam = appearances[
        ["playerID",
         "teamID",
         "yearID",
         "lgID",
         "G_all"]].groupby(["playerID", "teamID", "lgID", "yearID"]).sum().reset_index()
    appearances = None

    totalGamesPerPlayerPerTeam.sort_values(["playerID", "yearID"], inplace=True)

    totalGamesPerPlayerPerTeam = totalGamesPerPlayerPerTeam.merge(
        teams,
        left_on=["teamID", "lgID", "yearID"],
        right_on=["teamID", "lgID", "yearID"]
    )

    totalGamesPerPlayerPerTeam = totalGamesPerPlayerPerTeam[[
        "playerID", "teamName", "lgID", "yearID", "G_all", "state"
    ]]

    return totalGamesPerPlayerPerTeam


def getPitchingStats(playerIDs, pitchersPlayerIDs):
    pitchingStats = pandas.read_csv("baseballdatabank-2019.2/core/Pitching.csv")
    pitchingStats = pitchingStats[
        pitchingStats["playerID"].isin(playerIDs)
    ]
    pitchingStats = pitchingStats[
        pitchingStats["playerID"].isin(pitchersPlayerIDs)
    ]

    summableStats = pitchingStats[[
        "playerID", "yearID", "W", "L", "BB", "SO"
    ]].groupby(["playerID", "yearID"]).sum()
    averageStats = pitchingStats[[
        "playerID", "yearID", "ERA"
    ]].groupby(["playerID", "yearID"]).mean()

    pitchingStats = summableStats.merge(averageStats, on=["playerID", "yearID"])

    return pitchingStats


def getBattingStats(playerIDs, pitchersPlayerIDs):
    battingStats = pandas.read_csv("baseballdatabank-2019.2/core/Batting.csv")
    battingStats = battingStats[
        battingStats["playerID"].isin(playerIDs)
    ]
    battingStats = battingStats[
        ~battingStats["playerID"].isin(pitchersPlayerIDs)
    ]

    battingStats = battingStats[[
        "playerID", "yearID", "AB", "R", "H", "2B", "3B", "HR", "RBI",
        "SB", "CS", "BB", "SO", "IBB", "HBP", "SH", "SF", "GIDP"
    ]].groupby(["playerID", "yearID"]).sum()
    battingStats.reset_index(inplace=True)

    # calculate AVG
    AVG = battingStats["H"] / battingStats["AB"]
    AVG.loc[AVG.isnull()] = 0

    # calculate OPS
    TB = battingStats["H"] + 2 * battingStats["2B"] + 3 * battingStats["3B"] + 4 * battingStats["HR"]
    OBP = (battingStats["H"] + battingStats["BB"] + battingStats["HBP"]) \
        / (battingStats["AB"] + battingStats["BB"] + battingStats["SF"] + battingStats["HBP"])
    SLG = TB / battingStats["AB"]
    OPS = OBP + SLG

    battingStats = battingStats[["playerID", "yearID", "HR", "RBI"]]
    battingStats["AVG"] = AVG
    battingStats["OPS"] = OPS

    return battingStats


def getPlayers():
    players = pandas.read_csv("baseballdatabank-2019.2/core/People.csv")
    players = players[players["deathYear"].isnull()]
    players = players[["playerID", "bbrefID", "nameFirst", "nameLast", "birthDay",
                       "birthMonth", "birthYear", "bats", "throws"]]
    players.reset_index(inplace=True)

    playersImgSrcs = getPlayersImagesSrcs(players["bbrefID"])  # WARNING this takes a few minutes
    players["playerFotoSrc"] = [playersImgSrcs[player["bbrefID"]] for _, player in players.iterrows()]

    players = players[["playerID", "nameFirst", "nameLast", "birthDay",
                       "birthMonth", "birthYear", "playerFotoSrc", "bats", "throws"]]

    appearances = pandas.read_csv("baseballdatabank-2019.2/core/Appearances.csv")
    appearances = appearances[appearances["playerID"].isin(players["playerID"])]
    totalGamesPerPlayerPerPosition = appearances[
        ["playerID",
         "G_p",
         "G_c",
         "G_1b",
         "G_2b",
         "G_3b",
         "G_ss",
         "G_lf",
         "G_cf",
         "G_rf",
         "G_dh"]].groupby("playerID").sum()
    appearances = None

    positionPerPlayer = totalGamesPerPlayerPerPosition.idxmax(axis=1).to_frame("position").reset_index()
    totalGamesPerPlayerPerPosition = None

    players = players.merge(positionPerPlayer, on="playerID")

    # translate positions to human readable
    positionsTranslations = {
        "G_p": "Pitcher",
        "G_c": "Catcher",
        "G_1b": "First Baseman",
        "G_2b": "Second Baseman",
        "G_3b": "Third Baseman",
        "G_ss": "Shortstop",
        "G_lf": "Left Field",
        "G_cf": "Center Field",
        "G_rf": "Right Field",
        "G_dh": "Designated Hitter"
    }
    for original, transl in positionsTranslations.items():
        players.loc[players["position"] == original, "position"] = transl

    # translate bats and throws to human readable
    for column in ["bats", "throws"]:
        for side in ["R", "L"]:
            players.loc[players[column] == side, column] = "Right" if side == "R" else "Left"

    return players


def main(argc, argv):
    #players = getPlayers()
    #players.to_csv("playersInfo.csv")
    players = pandas.read_csv("playersInfo.csv")  # comment the two above lines and uncomment this one
                                                   #  if you already created the teamsInfo.csvFile

    #teams = getTeams()
    #teams.to_csv("teamsInfo.csv")
    teams = pandas.read_csv("teamsInfo.csv")  # comment the two above lines and uncomment this one
                                               #  if you already created the teamsInfo.csvFile

    print("generating player search")
    playerSearch, playerInfo = playerSearchAndInfo(players, teams)
    playerSearch.to_csv("../data/playerSearch.csv")
    playerInfo.to_csv("../data/playerInfo.csv")
    playerSearch = playerInfo = None

    print("calculating pitchers playerIDs")
    pitchersPlayerIDs = players[
        players["position"] == "Pitcher"
        ].reset_index()["playerID"]

    print("generating games per team (with state info)")
    gamesPerTeam(players["playerID"], teams).to_csv("../data/gamesPerTeam.csv")

    print("generating pitching stats")
    getPitchingStats(players["playerID"], pitchersPlayerIDs).to_csv("../data/pitchingStats.csv")

    print("generating batting stats")
    getBattingStats(players["playerID"], pitchersPlayerIDs).to_csv("../data/battingStats.csv")


if __name__ == "__main__":
    import sys
    sys.exit(main(len(sys.argv), sys.argv))
