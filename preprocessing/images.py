
import os
import requests
import threading

from bs4 import BeautifulSoup


class PlayerImgGetter(threading.Thread):

    def __init__(self, bbrefIDs, begin, end, storage):
        super(PlayerImgGetter, self).__init__()
        self.bbrefIDs = bbrefIDs
        self.begin = begin
        self.end = end
        self.storage = storage

    def run(self):
        for i in range(self.begin, self.end, 1):
            bbrefID = self.bbrefIDs[i]

            url = f"https://www.baseball-reference.com/players/{bbrefID[0]}/{bbrefID}.shtml"
            html = requests.get(url).text

            soup = BeautifulSoup(html, "html.parser")

            for img in soup.find_all("img"):
                if "headshots" in img.get("src", "") and len(img.get("class", [])) == 0:
                    self.storage[bbrefID] = img["src"]
                    continue


def getPlayersImagesSrcs(bbrefIDs):
    imgSrcs = {bbrefID: None for bbrefID in bbrefIDs}

    print(f"Getting images of {len(bbrefIDs)} players")

    numThreads = 4

    threads = [None] * numThreads
    playersPerThread = len(bbrefIDs) // numThreads
    begin = end = 0
    for i in range(numThreads):
        begin = end
        if i != numThreads - 1:
            end = end + playersPerThread
        else:
            end = len(bbrefIDs)

        threads[i] = PlayerImgGetter(bbrefIDs, begin, end, imgSrcs)

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

    return imgSrcs


class TeamImgGetter(threading.Thread):

    def __init__(self, teamIDBR, begin, end, storage):
        super(TeamImgGetter, self).__init__()
        self.teamIDBR = teamIDBR
        self.begin = begin
        self.end = end
        self.storage = storage

    def run(self):
        for i in range(self.begin, self.end, 1):
            teamIDBR = self.teamIDBR[i]

            url = f"https://www.baseball-reference.com/teams/{teamIDBR}/"
            html = requests.get(url).text

            soup = BeautifulSoup(html, "html.parser")

            for img in soup.find_all("img"):
                if "teamlogo" in img.get("class", []):
                    self.storage[teamIDBR] = img["src"]
                    continue


def getTeamsImagesSrcs(teamIDBRs):
    imgSrcs = {teamIDBR: None for teamIDBR in teamIDBRs}

    print(f"Getting images of {len(teamIDBRs)} teams")

    numThreads = 6

    threads = [None] * numThreads
    playersPerThread = len(teamIDBRs) // numThreads
    begin = end = 0
    for i in range(numThreads):
        begin = end
        if i != numThreads - 1:
            end = end + playersPerThread
        else:
            end = len(teamIDBRs)

        threads[i] = TeamImgGetter(teamIDBRs, begin, end, imgSrcs)

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

    return imgSrcs
