const urlVars = getUrlVars(),
      playerID = urlVars["playerID"];

let statisticsFiles = "data/",
    drawStatisticsFunction;
if (urlVars["isPitcher"] == "true") {
    statisticsFiles += "pitchingStats.csv";
    drawStatisticsFunction = drawPitchingStats;
    $("#metric1LabelText").text("ERA");
    $("#metric2LabelText").text("W-L");
    $("#metric3LabelText").text("K/BB");
}
else {
    statisticsFiles += "battingStats.csv";
    drawStatisticsFunction = drawBattingStats;
    $("#metric1LabelText").text("AVG");
    $("#metric2LabelText").text("OPS");
    $("#metric3LabelText").text("HR/RBI");
}

let gamesPerTeam,
    statistics,
    playerInfo,
    uStatePaths;

Promise.all([
    d3.csv("data/gamesPerTeam.csv"),
    d3.csv(statisticsFiles),
    d3.csv("data/playerInfo.csv"),
    d3.json("data/uStatePaths.json"),
]).then(data => {
    gamesPerTeam = data[0];
    gamesPerTeam = gamesPerTeam.filter(row => row.playerID == playerID);

    setupYearSliderFilter(gamesPerTeam);

    statistics = data[1];
    statistics = statistics.filter(row => row.playerID == playerID);

    playerInfo = data[2];
    playerInfo = playerInfo.filter(row => row.playerID == playerID);

    if (playerInfo.length == 0) {
        alert(
            "No player found with the given playerID!\n" +
            "You will be redirected to the main page."
        );
        window.location = "index.html";
    }

    uStatePaths = data[3];

    $(document).ready(function () {
        drawGraphGamesPerTeam(gamesPerTeam);
        drawStatisticsFunction(statistics);
        fillPlayerInformation(playerInfo[0]);
        drawGamesPerStateMap(gamesPerTeam);
    });
});

/**
 * Defines the behavior when the user applies the filter of a new year interval.
 * Call the draw functions again but with the data filtered by new year interval.
 */
$("#filterButton").click(function () {
    const begin = $("#slider-range").slider("values", 0),
          end = $("#slider-range").slider("values", 1);

    drawGraphGamesPerTeam(gamesPerTeam.filter((d) =>  d.yearID >= begin && d.yearID <= end));
    drawGamesPerStateMap(gamesPerTeam.filter((d) =>  d.yearID >= begin && d.yearID <= end));
    drawStatisticsFunction(statistics.filter((d) =>  d.yearID >= begin && d.yearID <= end));

    $("#filtrarModal").modal('hide')
})

/**
 * Redraw the statistics graph to display the new
 *  metric chosen
 */
$(".metricChosen").on('change', function() {
    drawStatisticsFunction(statistics);
});

/**
 * Draws the stacked bar chart to show the history of number of games played
 *  by each team a player played in
 * 
 * @param gamesPerTeam loaded, can be filtered, from gamesPerTeam.csv
 */
function drawGraphGamesPerTeam(gamesPerTeam) {
    // Create Dataset with proper shape
    const barData = [];

    for (let entry of gamesPerTeam) {
        barData.push({
            name: entry.teamName,
            stack: entry.yearID,
            value: entry.G_all,
        })
    }

    barData.sort((entry1, entry2) => {
        return entry1.stack - entry2.stack
    })

    $(document).ready(function () {
        $("#numberOfGamesPerTeam").html('');

        const stackedBar = britecharts.stackedBar(),
            chartTooltip = britecharts.tooltip(),
            container = d3.select('.bar-container');

        stackedBar
            .tooltipThreshold(600)
            .width($("#numberOfGamesPerTeam").width())
            .height(400)
            .grid('horizontal')
            .isAnimated(true)
            .stackLabel('stack')
            .nameLabel('name')
            .valueLabel('value')
            .betweenBarsPadding(0.3)
            //.colorSchema(britecharts.colors.colorSchemas.blueGreen)
            .on('customMouseOver', chartTooltip.show)
            .on('customMouseMove', function (dataPoint, topicColorMap, x, y) {
                chartTooltip.update(dataPoint, topicColorMap, x, y);

                const total = dataPoint.values.map(point => point.value).reduce((v1, v2) => v1 + v2, 0)
                $("#numberOfGamesPerTeam").find(".tooltip-title").text(`Number of Games - Total ${total}`);
            })
            .on('customMouseOut', chartTooltip.hide);


        container.datum(barData).call(stackedBar);

        chartTooltip
            .topicLabel('values')
            .nameLabel('stack')
            .title('Number of Games');

        // Note that if the viewport width is less than the tooltipThreshold value,
        // this container won't exist, and the tooltip won't show up
        const tooltipContainer = d3.select('.metadata-group');
        tooltipContainer.datum([]).call(chartTooltip);

    });

}

/**
 * Sets the metrics title and description for players associated with batting
 * 
 * @param statistics loaded, can be filtered, from battingStats.csv
 */
function drawBattingStats(statistics) {
    $("#statsOfPlayer").html('');

    const dataset = []
    let dates;
    switch ($(".metricChosen:checked").val()) {
        case "metric1": // AVG
            $("#metricName").text("Batting Average");
            $("#metricDescription").html(
                "One of the oldest and most universal tools to measure a hitter's success at the" +
                "plate, <strong>batting average</strong> is determined by dividing a player's" +
                "hits by his total at-bats for a number between zero (shown as .000) and one" +
                "(1.000). In recent years, the league-wide batting average has typically hovered" +
                "around .250.<br/>" +
                "The game's best hitters can surpass .300, and a handful of players throughout" +
                "history have even finished a season with a batting average higher than .400," +
                "meaning four hits in every 10 at-bats"
            );

            dates = []
            for (let entry of statistics) {
                const event = new Date(entry.yearID);
                dates.push({ date: event.toISOString(), value: entry.AVG })
            }
            dataset.push({
                topicName: "AVG",
                topic: 1,
                dates: dates
            });
            break;
        case "metric2": // OPS
            $("#metricName").text("On-base Plus Slugging");
            $("#metricDescription").html(
                "<strong>OPS</strong> adds on-base percentage and slugging percentage to get one" +
                "number that unites the two. It's meant to combine how well a hitter can reach" +
                "base, with how well he can hit for average and for power. As a result, OPS is" +
                "widely considered one of the best evaluative tools for hitters."
            );

            dates = [];
            for (let entry of statistics) {
                const event = new Date(entry.yearID);
                dates.push({ date: event.toISOString(), value: entry.OPS })
            }
            dataset.push({
                topicName: "OPS",
                topic: 1,
                dates: dates
            });
            break;
        case "metric3": // HR/RBI
            $("#metricName").text("Homeruns/Run Batted In");
            $("#metricDescription").html(
                "A <strong>home run</strong> occurs when a batter hits a fair ball and scores on" +
                "the play without being put out or without the benefit of an error.<br/>" +
                "In almost every instance of a home run, a batter hits the ball in the air over" +
                "the outfield fence in fair territory. In that situation, the batter is awarded" +
                "all four bases, and any runners on base score as well. The batter can circle" +
                "the bases at his leisure, as there is no threat of him being thrown out.<br/>" +
                "A batter is credited with an <strong>RBI</strong> in most cases where the" +
                "result of his plate appearance is a run being scored."
            );

            dates = [[],[]];
            for (let entry of statistics) {
                const event = new Date(entry.yearID);
                dates[0].push({ date: event.toISOString(), value: entry.HR })
                dates[1].push({ date: event.toISOString(), value: entry.RBI })
            }
            dataset.push({
                topicName: "HR",
                topic: 1,
                dates: dates[0]
            });
            dataset.push({
                topicName: "RBI",
                topic: 2,
                dates: dates[1]
            });
            break;
    }

    $("#statsOfPlayer").html('');

    drawStatisticsGraph(dataset);
}

/**
 * Sets the metrics title and description for players associated with pitching
 * 
 * @param statistics loaded, can be filtered, from pitchingStats.csv
 */
function drawPitchingStats(statistics) {
    const dataset = [];

    let dates;
    switch ($(".metricChosen:checked").val()) {
        case "metric1": // ERA
            $("#metricName").text("Earned Run Average")
            $("#metricDescription").html(
                "<strong>Earned run average</strong> represents the number of earned runs a" +
                "pitcher allows per nine innings -- with earned runs being any runs that scored" +
                "without the aid of an error or a passed ball. ERA is the most commonly accepted" +
                "statistical tool for evaluating pitchers.<br/>" +
                "The formula for finding ERA is: 9 x earned runs / innings pitched."
            )

            dates = []
            for (let entry of statistics) {
                const event = new Date(entry.yearID);
                dates.push({ date: event.toISOString(), value: entry.ERA })
            }
            dataset.push({
                topicName: "ERA",
                topic: 1,
                dates: dates
            });
            break;
        case "metric2": // W-L
            $("#metricName").text("Wins/Losses")
            $("#metricDescription").html(
                "A pitcher receives a <strong>win</strong> when he is the pitcher of record when" +
                "his team takes the lead for good.<br/>" +
                "A pitcher receives a <strong>loss</strong> when a run that is charged to him" +
                "proves to be the go-ahead run in the game, giving the opposing team a lead it" +
                "never gives up."
            );

            dates = [[],[]];
            for (let entry of statistics) {
                const event = new Date(entry.yearID);
                dates[0].push({ date: event.toISOString(), value: entry.W })
                dates[1].push({ date: event.toISOString(), value: entry.L })
            }
            dataset.push({
                topicName: "W",
                topic: 1,
                dates: dates[0]
            });
            dataset.push({
                topicName: "L",
                topic: 2,
                dates: dates[1]
            });
            break;
        case "metric3": // K/BB
            $("#metricName").text("Strikeout/Walk");
            $("#metricDescription").html(
                "A <strong>strikeout</strong> occurs when a pitcher throws any combination of" +
                "three swinging or looking strikes to a hitter. (A foul ball counts as a strike," +
                "but it cannot be the third and final strike of the at-bat.)<br/>" +
                "A <strong>walk</strong> (or <strong>base on balls</strong>) occurs when a" +
                "pitcher throws four pitches out of the strike zone, none of which are swung at" +
                "by the hitter. After refraining from swinging at four pitches out of the zone," +
                "the batter is awarded first base."
            );

            dates = [[],[]];
            for (let entry of statistics) {
                const event = new Date(entry.yearID);
                dates[0].push({ date: event.toISOString(), value: entry.SO })
                dates[1].push({ date: event.toISOString(), value: entry.BB })
            }
            dataset.push({
                topicName: "K",
                topic: 1,
                dates: dates[0]
            });
            dataset.push({
                topicName: "BB",
                topic: 2,
                dates: dates[1]
            });
            break;
    }

    $("#statsOfPlayer").html('');

    drawStatisticsGraph(dataset);
}

/**
 * Draws the line chart associated with a given player of a given statistic
 * 
 * @param dataset pitching or batting data
 */
function drawStatisticsGraph(dataset) {
    const lineChart = britecharts.line(),
          chartTooltip = britecharts.tooltip(),
          container = d3.select('.line-container'),
          width = Math.trunc($("#statsOfPlayer").width());

    lineChart
        .tooltipThreshold(600)
        .height(350)
        .margin({ bottom: 50 })
        .grid('vertical')
        .width(width)
        .on('customMouseOver', chartTooltip.show)
        .on('customMouseMove', function (dataPoint, topicColorMap, dataPointXPosition) {
            chartTooltip.update(dataPoint, topicColorMap, dataPointXPosition);
        })
        .on('customMouseOut', chartTooltip.hide);

    container.datum({ dataByTopic: dataset }).call(lineChart);

    chartTooltip
        .title('Metric');

    tooltipContainer = d3.select('.metadata-group .vertical-marker-container');
    tooltipContainer.datum([]).call(chartTooltip);
}

/**
 * Draws a color map to show the geographic distribution of played games of a player
 * 
 * Based on: https://gist.github.com/NPashaP/a74faf20b492ad377312
 */
function drawGamesPerStateMap(gamesPerTeam) {
    /**
     * Defines the content of a state's toltip
     * 
     * @param state state hovered
     * @param teamsHistory player's team history for the hovered state
     */
    function generateTooltip(state, teamsHistory) {
        let toolTipContent =
            `<span>${state}</span>` +
            "<div class='mapTooltipLine'></div>" +
            "<table>",
            total = 0;

        for (let entry of teamsHistory) {
            toolTipContent += "<tr>" +
                                `<td>${entry.team}</td>` +
                                `<td>${entry.year}</td>` +
                                `<td>${entry.games}</td>` +
                              "</tr>"
            total += entry.games;
        }

        toolTipContent +=   "<tr>" +
                                "<td><strong>Total</strong></td>" +
                                "<td></td>" +
                                `<td>${total}</td>` +
                            "</tr>" +
                          "</table>"

        return toolTipContent;
    }

    let gamesPerState = {};

    /**
     * Creates the tooltip when the user hovers over a state
     * 
     * @param state state hovered
     */
    function mouseOver(state) {
        if (state.id in gamesPerState) {
            d3.select("#mapTooltip")
                .style("opacity", 1)
                .html(generateTooltip(state.n, gamesPerState[state.id].history))
                .style("left", (d3.event.pageX) + "px")
                .style("top", (d3.event.pageY - 28) + "px");
        }
    }

    /**
     * Hides the state's tooltip
     * 
     * @param state state that the user left
     */
    function mouseOut(state) {
        if (state.id in gamesPerState) {
            d3.select("#mapTooltip")
                .style("opacity", 0);
        }
    }

    // build the necessary object to later fill the map
    for (let entry of gamesPerTeam) {
        if (!(entry.state in gamesPerState)) {
            gamesPerState[entry.state] = {
                color: null,
                history: [],
            };
        }

        gamesPerState[entry.state]["history"].push({
            team: entry.teamName,
            year: entry.yearID,
            games: parseInt(entry.G_all),
        })
    }

    // calculate total of games per state
    let totalPerState = {},
        max = -1;
    for (let [state, data] of Object.entries(gamesPerState)) {
        let teamsHistory = data.history;

        let total = 0;
        for (let historyEntry of teamsHistory) {
            total += historyEntry.games;
        }

        totalPerState[state] = total;
        if (total > max) {
            max = total;
        }
    }

    let colorInterpolator = d3.interpolate(
        "#ccf7f6",
        "#005e66",
    );

    // sort entries in state per year
    for (let [state, total] of Object.entries(totalPerState)) {
        gamesPerState[state].color = colorInterpolator(total / max);

        gamesPerState[state].history.sort((entry1, entry2) => {
            return parseInt(entry1.year) - parseInt(entry2.year);
        });
    }


    $("#map").attr("width", $("#map-card-body").width());
    $("#map").html(''); // clear the map. Used for filtering

    d3.select("#map").selectAll(".state")
        .data(uStatePaths)
        .enter()
        .append("path")
            .attr("class", "state")
            .attr("d", function (state) { return state.d; })
            .style("fill", function (state) {
                if (state.id in gamesPerState) {
                    return gamesPerState[state.id].color;
                }
                else {
                    return "rgb(198, 198, 198)";
                }
            })
            .on("mouseover", mouseOver)
            .on("mouseout", mouseOut);
}

/**
 * Gets all the variables on the url
 * 
 * From: https://html-online.com/articles/get-url-parameters-javascript/
 */
function getUrlVars() {
    const vars = {};
    window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (_, key, value) {
        vars[key] = value;
    });
    return vars;
}

/**
 * Fills the player's information row 
 * @param playerInfo loaded, can be filtered, from playerInfo.csv
 */
function fillPlayerInformation(playerInfo) {
    $("#playerName").text(`${playerInfo.nameFirst} ${playerInfo.nameLast}`);
    $("#playerTeam").text(playerInfo.teamName);

    const birthDay = parseInt(playerInfo.birthDay) + "",
          birthMonth = parseInt(playerInfo.birthMonth) + "";
    $("#playerBorn").text(
        `${birthDay.length == 1 ? "0" : ""}${birthDay}/` +
        `${birthMonth.length == 1 ? "0" : ""}${birthMonth}/` +
        `${parseInt(playerInfo.birthYear)}`
        );
    $("#playerPosition").text(playerInfo.position);
    $("#playerBats").text(playerInfo.bats);
    $("#playerThrows").text(playerInfo.throws);

    if (playerInfo.playerFotoSrc) {
        $("#playerFace").attr("src",playerInfo.playerFotoSrc);
    }

    if (playerInfo.teamLogoSrc) {
        $("#playerTeamLogo").attr("src",playerInfo.teamLogoSrc);
    }
}

/**
 * Setups the slider that will be used to filter the player's data.
 * On this function it is calculated the time interval were players was
 *  active. The slider will only allow filtering for this time interval.
 * @param gamesPerTeam loaded, can be filtered, from gamesPerTeam.csv
 */
function setupYearSliderFilter(gamesPerTeam) {
    let minYear = Infinity,
        maxYear = -Infinity;
    for (const yearStats of gamesPerTeam) {
        const year = parseInt(yearStats["yearID"]);
        if (year < minYear) {
            minYear = year;
        }
        if (year > maxYear) {
            maxYear = year;
        }
    }

    // https://jqueryui.com/slider/
    $("#slider-range").slider(
        {
            range: true,
            min: minYear,
            max: maxYear,
            values: [minYear, maxYear],
            slide: function (event, ui) {
                $("#amount").val(ui.values[0] + " - " + ui.values[1]);
            }
        }
    );

    // set the content of the year interval label on the modal
    $("#amount").val(`${minYear}-${maxYear}`);
}