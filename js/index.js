d3.csv("data/playerSearch.csv").then(players => {
    d3.select('tbody').selectAll("tr")
        .data(players)
        .enter()
        .append("tr")
            .attr("class", "clickable-row")
            .attr("data-href", row =>
                `player.html?playerID=${row.playerID}&isPitcher=${row.position == "Pitcher" ? true: false}`
            )
            .selectAll("td")
            .data(row => {
                const name= `${row.nameFirst} ${row.nameLast}`;
                return [name, row.teamName, row.lgID, row.position, row.state]
            })
            .enter()
            .append("td")
                .text(content => content);

    $('#player-list-table').DataTable(
        {
            aLengthMenu: [ // defines the possibel values for the players per page dropdown
                [5, 10, 15, -1],
                [5, 10, 15, "All"],
            ],
            iDisplayLength: 10,  // number of players per page by default
            language: {
                sSearch: "Filter: " // label for the search input
            },
            deferRender: true, // speeds initialization
            searching: true, // allow search
            paging: true, // enable paging
            info: false, // removes redundant pages and content size information
        }
    );

    $("#player-list-table").on('click', '.clickable-row', function() {
        window.location = $(this).data("href");
    });

    $("#table-div").animate({
        opacity: "+=2"
    }, 1000);
    $("#loading").animate({
        opacity: "-=10",
        height: "-=50"
    }, 1000)
})