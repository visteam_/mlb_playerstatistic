Paginas construídas usando como base o template disponivel em http://www.urbanui.com/majestic/template/index.html

Pastas:
css: Ficheiros stylesheet files.
     Apenas a parte final do ficheiro foi adicionada por nós.
     O restante já estava presente com o template

images: Contém o logo do site, e images para servir de placeholder caso
         um jogador ou equipa não tenha imagem associada.

js: Ficheiros que tratam da lógica das duas páginas criadas

preprocessing: Scripts para fazer o preprocessamento dos dados

scss: Syntactically Awesome Style Sheets.
      Já presente no template. Não alteramos

vendors: Bibliotecas externas
          - base: ficheiros já existentes no template.
                  contem:
                   - bootstrap
                   - jquery
          - britecharts: para criear o gráfico de linhas e de barras-stacked
                         https://eventbrite.github.io/britecharts/
          - d3: usado para manipular os dados, construir o mapa e a tabela com a lista de jogadores
                         https://d3js.org/
          - datatables: possibilita a pesquisa e ordenação na tabela com a lista de jogadores
                        https://www.datatables.net/
          - jquery-ui: usado para criar o slider para filtrar os dados por um intervalo de anos
                        https://jqueryui.com/
          - mdi: icons. Já era presente no template